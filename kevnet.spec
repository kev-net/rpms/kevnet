Name:		kevnet
Version:	%{ci_version}
Release:	%{ci_release}%{?dist}
Summary:	KevNet package repository

License:	Apache-2.0
URL:		https://gitlab.com/kev-net/rpms/kevnet/
Source0:	kevnet.repo
Source1:	hashicorp.repo
Source2:	kubernetes.repo
Source3:	libexec

BuildRequires:	coreutils, sed
Requires:	ca-certificates, coreutils, policycoreutils, gzip, util-linux, systemd, shadow-utils, bash

%description
The kevnet package contains the files for accessing the KevNet package
repository.


%install
mkdir -p %{buildroot}/%{_sysconfdir}/yum.repos.d
sed -s 's|@ETC@|%{_sysconfdir}|g' < %{SOURCE0} > %{buildroot}/%{_sysconfdir}/yum.repos.d/kevnet.repo
mkdir -p %{buildroot}/%{_sysconfdir}/pki/rpm-gpg
cp %{kn_env_rpm_public_key} %{buildroot}/%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-kevnet
cp %{SOURCE1} %{buildroot}/%{_sysconfdir}/yum.repos.d/hashicorp.repo
sed -s 's|@K8S_VERS@|%{kn_env_kubernetes_version}|g' < %{SOURCE2} > %{buildroot}/%{_sysconfdir}/yum.repos.d/kubernetes.repo
mkdir -p %{buildroot}/%{_datadir}/pki/ca-trust-source/anchors
cp %{kn_env_kevnet_root_ca} %{buildroot}/%{_datadir}/pki/ca-trust-source/anchors/kevnet-ca.pem
mkdir -p %{buildroot}/%{_sysconfdir}/kevnet
mkdir -p %{buildroot}/%{_libexecdir}/kevnet
for lib in %{SOURCE3}/*; do
    sed -s 's|@LIBEXEC@|%{_libexecdir}/kevnet|g' < "${lib}" > %{buildroot}/%{_libexecdir}/kevnet/$(basename "${lib}")
done


%post
update-ca-trust


%postun
update-ca-trust


%files
%defattr(644,root,root)
%license LICENSE
%doc README.md
%{_sysconfdir}/yum.repos.d/kevnet.repo
%{_sysconfdir}/yum.repos.d/hashicorp.repo
%{_sysconfdir}/yum.repos.d/kubernetes.repo
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-kevnet
%{_datadir}/pki/ca-trust-source/anchors/kevnet-ca.pem
%dir %attr(755,root,root) %{_sysconfdir}/kevnet
%dir %attr(755,root,root) %{_libexecdir}/kevnet
%{_libexecdir}/kevnet/*


%changelog
* Sun Nov  5 2023 Kevin L. Mitchell <klmitch@mit.edu>
- Add kubernetes repo, as well as kevnet directory

* Fri May 12 2023 Kevin L. Mitchell <klmitch@mit.edu>
- Add KevNet root CA

* Wed Feb 15 2023 Kevin L. Mitchell <klmitch@mit.edu>
- Add hashicorp repo as well, as that will be required going forward

* Wed Sep 21 2022 Kevin L. Mitchell <klmitch@mit.edu>
- Initial creation of the RPM
