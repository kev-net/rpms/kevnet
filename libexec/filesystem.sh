# fs_mkdir [<options>] <directory>
#
# Creates the specified directory.  Use -m/--mode to specify the mode,
# -o/--owner to specify user and group owning the directory, and
# -r/--restorecon signals to run the restorecon utility after the
# directory is created.  If the directory already exists, its mode and
# ownership will be adjusted as directed.
fs_mkdir() {
    local mode=0755
    local owner restorecon fname
    if ! argparse -n fs_mkdir \
         -a "-m --mode/arg_set mode" \
         -a "-o --owner/arg_set owner" \
         -f "-r --restorecon/arg_set restorecon true" \
         "arg_set fname" -- "$@"; then
        return 1
    fi

    # Make the directory if it doesn't exist, or fix its mode if it
    # does
    if [ ! -e "${fname}" ]; then
        dbexec mkdir -m "${mode}" "${fname}"
    elif [ ! -d "${fname}" ]; then
        log -l fatal "fs_mkdir: Directory \"${fname}\" exists but is not a directory"
    else
        dbexec chmod "${mode}" "${fname}"
    fi

    # Handle the ownership and restorecon phases
    if [ -n "${owner}" ]; then
        dbexec chown "${owner}" "${fname}"
    fi
    if [ "${restorecon}" == true ]; then
        dbexec restorecon -R "${fname}"
    fi

    log "Directory \"${fname}\" created"
}

# fs_mkfile [<options>] <file>
#
# Creates the specified file, overwriting it if necessary.  Use
# -m/--mode to specify the mode, -o/--owner to specify the user and
# group owning the directory, and -r/--restorecon signals to run the
# restorecon utility after the file is created.  If the file exists,
# it will be overwritten, unless -O/--no-overwrite is passed.  The
# content of the file comes from standard input; use -b/--base64 to
# indicate it is base64-encoded, or -z/--gzip to indicate it is
# gzip-compressed (and base64 encoded).  The gzip decoding option
# takes precedence over the base64 encoding option.
fs_mkfile() {
    local mode=0644
    local overwrite=true
    local base64 gzip owner restorecon fname
    if ! argparse -n fs_mkfile \
         -a "-m --mode/arg_set mode" \
         -a "-o --owner/arg_set owner" \
         -f "-b --base64/arg_set base64 true" \
         -f "-z --gzip/arg_set gzip true" \
         -f "-O --no-overwrite/arg_set overwrite false" \
         -f "-r --restorecon/arg_set restorecon true" \
         "arg_set fname" -- "$@"; then
        return 1
    fi

    # Check if the file exists or if we're overwriting it, then write
    # content to the file
    if [ -e "${fname}" ] && [ ! -f "${fname}" ]; then
        log -l fatal "fs_mkfile: File \"${fname}\" exists but is not a file"
    elif [ "${overwrite}" == true ] || [ ! -e "${fname}" ]; then
        # Pick the correct output
        if [ "${KN_DEBUG}" != false ]; then
            exec 6>"${fname}"
        else
            exec 6>&2
        fi

        # Select the correct input decoding
        if [ "${gzip}" == true ]; then
            log -l debug "Decoding gzip-compressed content to \"${fname}\""
            base64 -d | gzip -d >&6
        elif [ "${base64}" == true ]; then
            log -l debug "Decoding base64-encoded content to \"${fname}\""
            base64 -d >&6
        else
            log -l debug "Writing content to \"${fname}\""
            cat >&6
        fi

        # Close the output
        exec 6<&-

        # If debugging, output the content
        if [ "${KN_DEBUG}" == true ]; then
            cat "${fname}" >&2
        fi
    else
        # Consume the input
        log -l debug "Discarding content in favor of existing \"${fname}\""
        cat >/dev/null
    fi

    # Handle the mode, ownership, and restorecon phases
    dbexec chmod "${mode}" "${fname}"
    if [ -n "${owner}" ]; then
        dbexec chown "${owner}" "${fname}"
    fi
    if [ "${restorecon}" == true ]; then
        dbexec restorecon "${fname}"
    fi

    log "File \"${fname}\" created"
}

# fs_append <file> <content>
#
# A utility function to append the specified content as a specified
# line to the specified file.  No attempt is made to detect if the
# content is already in the file, or to even detect if the file
# exists.
fs_append() {
    local fname=$1
    shift
    log -l debug "Appending \"$*\" to \"${fname}\""
    if [ "${KN_DEBUG}" != false ]; then
        echo "$*" >>"${fname}"
    fi
}
