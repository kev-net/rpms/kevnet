# svc_start <service> [...]
#
# Enables and starts the specified services.
svc_start() {
    local svc
    for svc in "$@"; do
        dbexec systemctl enable "${svc}"
        dbexec systemctl start "${svc}"
        log "Service \"${svc}\" enabled and started"
    done
}

# svc_restart <service> [...]
#
# Restarts the specified service, if it is running.  This uses the
# systemctl "try-restart" command, so will not restart services that
# are not running.
svc_restart() {
    local svc
    for svc in "$@"; do
        dbexec systemctl try-restart "${svc}"
        log "Service \"${svc}\" restarted"
    done
}
