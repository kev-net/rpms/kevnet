# Directory containing the KevNet shell library files.
_kn_libexec=${KN_LIBEXEC:-@LIBEXEC@}

# Map to prevent multiple inclusion.
declare -A _kn_reqs=([kevnet]=true)

# arg_set <name> <value>
#
# An argparse action that sets the named variable to the given value.
arg_set() {
    local -n _var=$1
    shift

    _var=$*
}

# arg_clear <name>
#
# An argparse action that clears the named variable.
arg_clear() {
    unset "$1"
}

# arg_push <name> <value>
#
# An argparse action that adds a value to a named array variable.
arg_push() {
    local -n _var=$1
    shift

    _var[${#_var[@]}]=$*
}

# arg_append <name> <delimiter> <value>
#
# Appends a value to a string, separated by the specified delimiter.
arg_append() {
    # shellcheck disable=SC2178
    local -n _var=$1
    local _delim=$2
    shift 2

    # shellcheck disable=SC2128
    if [ -z "${_var}" ]; then
        _var=$*
    else
        _var="${_var}${_delim}$*"
    fi
}

# arg_error <code> <message>
#
# An argparse action that emits an error message to the standard error
# output and returns the specified error code.
arg_error() {
    local _code=$1
    shift

    # Emit the message
    echo "$@" >&2

    return "${_code}"
}

# _arg_opt <short> <long> <actmap> <argmap> <suffix> <names> <action>
#
# Set up the option tracking variables.
_arg_opt() {
    local -n __short=$1
    local -n __long=$2
    local -n __actmap=$3
    local -n __argmap=$4
    local _suffix=$5
    local _names=$6
    local _action=$7

    # Loop through the names
    local _name
    for _name in ${_names}; do
        # Handle the common processing
        # shellcheck disable=SC2004
        __actmap[${_name}]=${_action}
        if [ -n "${_suffix}" ]; then
            # shellcheck disable=SC2004
            __argmap[${_name}]=true
        fi

        # Handle the option styles separately
        case "${_name}" in
            --*)
                arg_append __long ',' "${_name:2}${_suffix}"
                ;;
            -*)
                __short="${__short}${_name:1}${_suffix}"
                ;;
        esac
    done
}

# argparse [<options>] -- <arguments>
#
# Parses arguments according to the provided options.
argparse() {
    local _name=$0
    local _short _long
    local -A _actmap _argmap
    local -a _pos
    local _repeat=false

    # Process the argument descriptions
    while [ $# -gt 0 ]; do
        case "$1" in
            -n | --name) # Specify name of the function
                shift
                _name=$1
                ;;
            -n*)
                _name=${1:2}
                ;;
            --name=*)
                _name=${1:7}
                ;;

            -f | --flag) # Specify a flag, taking no arguments
                shift
                _arg_opt _short _long _actmap _argmap '' \
                         "$(echo "$1" | cut -f1 -d/)" \
                         "$(echo "$1" | cut -f2 -d/)"
                ;;
            -f*)
                _arg_opt _short _long _actmap _argmap '' \
                         "$(echo "${1:2}" | cut -f1 -d/)" \
                         "$(echo "${1:2}" | cut -f2 -d/)"
                ;;
            --flag=*)
                _arg_opt _short _long _actmap _argmap '' \
                         "$(echo "${1:7}" | cut -f1 -d/)" \
                         "$(echo "${1:7}" | cut -f2 -d/)"
                ;;

            -a | --arg) # Specify an option taking an argument
                shift
                _arg_opt _short _long _actmap _argmap ':' \
                         "$(echo "$1" | cut -f1 -d/)" \
                         "$(echo "$1" | cut -f2 -d/)"
                ;;
            -a*)
                _arg_opt _short _long _actmap _argmap ':' \
                         "$(echo "${1:2}" | cut -f1 -d/)" \
                         "$(echo "${1:2}" | cut -f2 -d/)"
                ;;
            --arg=*)
                _arg_opt _short _long _actmap _argmap ':' \
                         "$(echo "${1:6}" | cut -f1 -d/)" \
                         "$(echo "${1:6}" | cut -f2 -d/)"
                ;;

            -o | --option) # Specify an option taking optional arguments
                shift
                _arg_opt _short _long _actmap _argmap '::' \
                         "$(echo "$1" | cut -f1 -d/)" \
                         "$(echo "$1" | cut -f2 -d/)"
                ;;
            -o*)
                _arg_opt _short _long _actmap _argmap '::' \
                         "$(echo "${1:2}" | cut -f1 -d/)" \
                         "$(echo "${1:2}" | cut -f2 -d/)"
                ;;
            --option=*)
                _arg_opt _short _long _actmap _argmap '::' \
                         "$(echo "${1:9}" | cut -f1 -d/)" \
                         "$(echo "${1:9}" | cut -f2 -d/)"
                ;;

            -r | --repeat) # Repeat last positional action
                _repeat=true
                ;;

            --) # End of option processing
                shift
                break
                ;;

            *) # Specify positional arguments
                arg_push _pos "$1"
                ;;
        esac
        shift
    done

    # Make sure things make sense
    if [ "${#_pos[@]}" -eq 0 ] && [ "${_repeat}" == true ]; then
        echo "argparse: Cannot repeat last positional action: no actions" >&2
        return 2
    fi

    # Parse the arguments using getopt and put in _args
    local _tmp
    _tmp=$(getopt -o "${_short}" -l "${_long}" -n "${_name}" -- "$@")
    # shellcheck disable=SC2181
    if [ $? -ne 0 ]; then
        return 1
    fi
    eval "local -a _args=(${_tmp})"
    local _idx=0

    # Process the provided options
    while true; do
        # shellcheck disable=SC2154
        local _opt=${_args[${_idx}]}
        _idx=$((_idx + 1))
        case "${_opt}" in
            --) # End of options
                break
                ;;

            *) # Option
                if [ "${_argmap[${_opt}]}" == true ]; then
                    local _arg=${_args[${_idx}]}
                    _idx=$((_idx + 1))
                    eval "${_actmap[${_opt}]} \"${_arg}\""
                else
                    eval "${_actmap[${_opt}]}"
                fi
                ;;
        esac
    done

    # Handle positional arguments
    local _pidx=0
    while [ "${_pidx}" -lt "${#_pos[@]}" ]; do
        if [ "${_idx}" -ge "${#_args[@]}" ]; then
            echo "${_name}: Missing required positional argument(s)" >&2
            return 1
        fi
        local _arg=${_args[${_idx}]}
        _idx=$((_idx + 1))
        eval "${_pos[${_pidx}]} \"${_arg}\""
        _pidx=$((_pidx + 1))
    done

    # Handle left-over arguments
    if [ "${_idx}" -lt "${#_args[@]}" ]; then
        if [ "${_repeat}" == true ]; then
            local _action=${_pos[$((${#_pos[@]} - 1))]}
            while [ "${_idx}" -lt "${#_args[@]}" ]; do
                eval "${_action} \"${_args[${_idx}]}\""
                _idx=$((_idx + 1))
            done
        else
            echo "${_name}: Unexpected positional argument(s)" >&2
            return 1
        fi
    fi
}

# log [<options>] <message>
#
# Emits logging information.  The -l/--level option specifies the log
# level, with "debug" suppressed unless KN_DEBUG is set;
# -t/--timestamp adds a timestamp to the log message.
log() {
    local level=info
    local timestamp=${KN_LOG_TIMESTAMP:+true}
    local msg fatal
    if ! argparse -n log \
         -a "-l --level/arg_set level" \
         -f "-t --timestamp/arg_set timestamp true" \
         "arg_append msg ' '" -r -- "$@"; then
        return 0
    fi

    # Handle special-cased levels
    if [ "${level@U}" == "DEBUG" ] && [ -z "${KN_DEBUG}" ]; then
        return 0
    elif [ "${level@U}" == "FATAL" ]; then
        level=error
        fatal=true
    fi

    # Set up the message
    msg="[${level@U}] ${msg}"
    if [ "${timestamp}" == true ]; then
        msg="$(date -Iseconds) ${msg}"
    fi

    # Emit the message
    echo "${msg}" >&2

    # Was it fatal?
    if [ "${fatal}" == true ]; then
        exit 1
    fi
}

# dbexec <command>
#
# Executes a command.  If KN_DEBUG is "true", the command being
# executed is emitted as a DEBUG-level log as well as being executed.
# If KN_DEBUG is "false", the command is only emitted as a DEBUG-level
# log, and not executed.
dbexec() {
    log -l debug ">>> ${*@Q}"
    if [ "${KN_DEBUG}" == false ]; then
        return 0
    fi
    "$@"
}

# kn_req <library> [...]
#
# Includes one or more KevNet libraries by name.
kn_req() {
    local lib
    for lib in "$@"; do
        # Does the library exist?
        if [ ! -r "${_kn_libexec}/${lib}.sh" ]; then
            log -l fatal "No such library \"${lib}\""
        fi

        # Has it already been included?
        if [ -n "${_kn_reqs[${lib}]}" ]; then
            log -l debug "Library \"${lib}\" already included"
            continue
        fi
        _kn_reqs[${lib}]=true

        log -l debug "Including library \"${lib}\""

        # shellcheck disable=SC1090
        . "${_kn_libexec}/${lib}.sh"
    done
}
