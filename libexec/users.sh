# Need the filesystem functions for this.
kn_req filesystem

# grp_make [<options>] <group>
#
# Creates the specified group.  The -g/--gid option specifies the
# group ID to use; the -U/--users option specifies a user to add to
# that group, and can be given multiple times.
grp_make() {
    local gid gname
    local users
    if ! argparse -n grp_make \
         -a "-g --gid/arg_set gid" \
         -a "-U --users/arg_append users ','" \
         "arg_set gname" -- "$@"; then
        return 1
    fi

    # If the group already exists, do nothing
    if grep -q "${gname}" /etc/group; then
        log "Group \"${gname}\" already exists"
        return 0
    fi

    # Set up the arguments for groupadd
    local -a opts
    if [ -n "${gid}" ]; then
        arg_push opts "-g"
        arg_push opts "${gid}"
    fi
    if [ -n "${users}" ]; then
        arg_push opts "-U"
        arg_push opts "${users}"
    fi

    # Create the group
    dbexec groupadd "${opts[@]}" "${gname}"
    log "Group \"${gname}\" created."
}

# usr_make [<options>] <user>
#
# Creates the specified user and the user's group.  The -g/--group
# option specifies the name of the user group to create (defaults to
# <user>), while -G/--gid specifies the group ID for the user group.
# The -c/--comment option specifies a comment for the user; -h/--home
# specifies the user's home directory (defaults to /home/<user>);
# -s/--shell specifies the user's shell (defaults to /bin/bash);
# -u/--uid specifies the user's ID.  The -M/--no-create-home flag
# specifies that the user's home directory should not be created; the
# -S/--ssh flag specifies that the authorized_keys should be
# initialized from root's authorized_keys (disabled if
# -M/--no-create-home is given); and -U/--sudo enables sudo access for
# the user.
usr_make() {
    local group gid groups comment home uid ssh sudo uname
    local shell=/bin/bash
    local mkhome=true
    if ! argparse -n usr_make \
         -a "-g --group/arg_set group" \
         -a "-G --gid/arg_set gid" \
         -a "-r --groups/arg_append groups ','" \
         -a "-c --comment/arg_set comment" \
         -a "-h --home/arg_set home" \
         -a "-s --shell/arg_set shell" \
         -a "-u --uid/arg_set uid" \
         -f "-M --no-create-home/arg_set mkhome false" \
         -f "-S --ssh/arg_set ssh true" \
         -f "-U --sudo/arg_set sudo true" \
         "arg_set uname" -- "$@"; then
        return 1
    fi

    # Set up some defaults
    if [ -z "${group}" ]; then
        group=${uname}
    fi
    if [ -z "${home}" ]; then
        home="/home/${uname}"
    fi

    # If the user already exists, do nothing
    if grep -q "${uname}" /etc/passwd; then
        log "User \"${uname}\" already exists"
        return 0
    fi

    # Create the group
    local -a group_opts
    if [ -n "${gid}" ]; then
        arg_push group_opts "-g"
        arg_push group_opts "${gid}"
    fi
    grp_make "${group_opts[@]}" "${group}"

    # Create the user
    local -a opts=(-g "${group}" -d "${home}" -s "${shell}")
    if [ -n "${groups}" ]; then
        arg_push opts "-G"
        arg_push opts "${groups}"
    fi
    if [ -n "${comment}" ]; then
        arg_push opts "-c"
        arg_push opts "${comment}"
    fi
    if [ -n "${uid}" ]; then
        arg_push opts "-u"
        arg_push opts "${uid}"
    fi
    if [ "${mkhome}" == false ]; then
        arg_push opts "-M"
    fi
    dbexec useradd "${opts[@]}" "${uname}"
    log "User \"${uname}\" created."

    # Prepare the SSH authorized_keys file
    if [ "${mkhome}" == true ] && [ "${ssh}" == true ]; then
        local sshdir="${home}/.ssh"
        fs_mkdir -m 0700 -o "${uname}:${group}" -r "${sshdir}"
        awk '{printf %s %s %s\n", $(NF-2), $(NF-1), $NF}' \
            < /root/.ssh/authorized_keys | \
            fs_mkfile -m 0600 -o "${uname}:${group}" -r \
                      "${sshdir}/authorized_keys"
        log "User \"${uname}\" SSH authorized_keys created"
    fi

    # Grant sudo access
    if [ "${sudo}" == true ]; then
        echo "${uname} ALL=(ALL) NOPASSWD:ALL" | \
            fs_mkfile -m 0640 "/etc/sudoers.d/10-${uname}"
        log "User \"${uname}\" granted sudo access"
    fi
}
